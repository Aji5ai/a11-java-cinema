import java.util.Arrays;
import java.util.List;

class Cinema {
    public static void main(String[] args) {
        String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
        int[] hoursWorked = { 35, 38, 35, 38, 40 };
        double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
        String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };

        for (int i = 0; i < hoursWorked.length; ++i) {
            System.out.print("Salaire hebdo de " + employeeNames[i] + ": ");
            double hoursByRates;
            if (hoursWorked[i] <= 35) {
                hoursByRates = hoursWorked[i] * hourlyRates[i];
            } else {
                hoursByRates = (35 * hourlyRates[i]) + (hoursWorked[i] - 35) * 1.5 * hourlyRates[i];
            }
            System.out.println(hoursByRates);
        }

        String searchPosition = "Caissier";
        List<String> list = Arrays.asList(positions);
        if (list.contains(searchPosition)) {
            for (int i = 0; i < positions.length; ++i) {
                if (positions[i].equals(searchPosition)) {
                    System.out.println(employeeNames[i]);
                }
            }
        } else {
            System.out.println("Aucun employé trouvé.");
        }
    }

}